import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class AtomicCounterTutor{
    private AtomicInteger counter = new AtomicInteger(0);

    private class TestThread implements Runnable {
        String threadName;

        TestThread(String threadName) {
            this.threadName = threadName;
        }

        @Override
        public void run() {
            for (int i = 0; i < 5; i++) {
                counter.incrementAndGet();
                System.out.println("I'm thread " + threadName + " count value = " + counter.get() + " and my i = " + i);
                Thread.yield();
            }
        }
    }

    @Test
    public void testThread() {
        List<Thread> threads = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            threads.add(new Thread(new TestThread("t" + i)));
        }
        System.out.println("Starting threads");
        for (int i = 0; i < 5; i++) {
            threads.get(i).start();
        }
        try {
            for (int i = 0; i < 5; i++) {
                threads.get(i).join();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Counter = " + counter);

    }
}
