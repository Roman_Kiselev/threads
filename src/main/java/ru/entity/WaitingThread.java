package ru.entity;

public class WaitingThread implements Runnable {
    private Object sync;
    private Data data;

    public WaitingThread(Object sync, Data data) {
        this.sync = sync;
        this.data = data;
    }

    @Override
    public void run() {
        System.out.println("own:: Thread started");
        synchronized(sync) {
            if (data.value == 0) {
                try {
                    System.out.println("own:: Waiting");
                    sync.wait();
                    System.out.println("own:: Running again");
                } catch (InterruptedException ex) {
                    System.err.println("own:: Interrupted: " + ex.getMessage());
                }
            }
            System.out.println("own:: data.value = " + data.value);
        }
    }
}
