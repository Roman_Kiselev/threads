package ru.entity;

public class A {
    private int value = 0;

    synchronized void setValue(int value){
        this.value = value;
    }

    private synchronized int getValue(){
        return value;
    }

    public synchronized boolean equals(Object o){
        A a = (A) o;
        try{
            Thread.sleep(1000);
        }catch(InterruptedException ex){
            System.err.println("Interrupted!");
        }
        return value == a.getValue();
    }
}
