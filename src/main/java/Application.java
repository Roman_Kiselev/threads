import ru.entity.A;
import ru.entity.Tester;

public class Application {
    public static void main(String[]arg){
        A a1 = new A();
        A a2 = new A();
        Thread t1 = new Thread(new Tester(a1,a2));
        Thread t2 = new Thread(new Tester(a2,a1));
        t1.start();
        t2.start();
    }
}
