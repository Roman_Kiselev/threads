import ru.entity.Data;
import ru.entity.WaitingThread;

public class ApplicationWaiting {
    public static void main(String[]arg){
        Object sync = new Object();
        Data data = new Data();
        Thread t = new Thread(new WaitingThread(sync, data));
        t.start();
        try{
            System.out.println("main::Sleeping");
            Thread.sleep(500);
        }catch(InterruptedException ex){
            System.err.println("main::Interrupted: "+ex.getMessage());
        }
        synchronized (sync){
            System.out.println("main::setting value to 1");
            data.value = 1;
            System.out.println("main::notifying thread");
            sync.notify();
            System.out.println("main::Thread notified");
        }
    }
}
